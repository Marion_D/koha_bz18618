# Portuguese translations for Koha package.
# Copyright (C) 2018 THE Koha'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Koha package.
# Automatically generated, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha 21.05\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-14 09:06-0300\n"
"PO-Revision-Date: 2021-11-16 17:55+0000\n"
"Last-Translator: vfernandes <vfernandes@keep.pt>\n"
"Language-Team: none\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1637085341.997511\n"
"X-Pootle-Path: /pt/21.05/pt-PT-messages.po\n"
"X-Pootle-Revision: 1\n"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:49
#, perl-brace-format
msgid "(search field {field_name} with mapping {marc_field}.)"
msgstr "(campo de pesquisa {field_name} com o mapeamento {marc_field}.)"

#: koha-tmpl/intranet-tmpl/prog/en/includes/patron-age.inc:9
#, perl-brace-format
msgid "({age} year)"
msgid_plural "({age} years)"
msgstr[0] "({age} ano)"
msgstr[1] "({age} anos)"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:48
msgid ""
"An error occurred when deleting the existing mappings. Nothing has been "
"changed!"
msgstr ""
"Ocorreu um erro ao eliminar os mapeamentos existentes. Não houve "
"modificações!"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:55
#, perl-brace-format
msgid ""
"An error occurred when updating Elasticsearch index mappings: {message}."
msgstr ""
"Ocorreu um erro ao atualizar os mapeamentos dos índices do ElasticSearch: "
"{message}."

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:46
#, perl-brace-format
msgid "An error occurred when updating mappings: {message}."
msgstr "Ocorreu um erro ao atualizar os mapeamentos: {message}."

#: koha-tmpl/opac-tmpl/bootstrap/en/includes/holds-table.inc:189
msgctxt "Cancel hold button"
msgid "Cancel"
msgstr "Cancelar"

#: koha-tmpl/intranet-tmpl/prog/en/modules/members/tables/members_results.tt:20
msgid "Check out"
msgstr "Emprestar"

#: koha-tmpl/intranet-tmpl/prog/en/modules/members/memberentrygen.tt:1326
msgctxt "patron restriction created on"
msgid "Created"
msgstr "Criado"

#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/booksellers.tt:105
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/transferorder.tt:44
msgctxt "basket created by"
msgid "Created by"
msgstr "Criado por"

#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/basket.tt:245
msgctxt "basket created by"
msgid "Created by:"
msgstr "Criado por:"

#: koha-tmpl/intranet-tmpl/prog/en/modules/suggestion/suggestion.tt:242
#: koha-tmpl/intranet-tmpl/prog/en/modules/suggestion/suggestion.tt:500
msgctxt "purchase suggestion created by"
msgid "Created by:"
msgstr "Criado por:"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:65
msgid "Elasticsearch is currently disabled."
msgstr "O Elasticsearch não se encontra ativo."

#: koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt:944
msgid "Hold"
msgid_plural "Holds"
msgstr[0] "Reserva"
msgstr[1] "Reservas"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:59
#, perl-brace-format
msgid "Index '{index}' needs to be recreated."
msgstr "O índice '{index}' necessita de ser recriado."

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:57
#, perl-brace-format
msgid "Index '{index}' needs to be reindexed."
msgstr "O índice '{index}' necessita de ser reindexado."

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:51
#, perl-brace-format
msgid "Invalid field weight '{weight}', must be a positive decimal number."
msgstr ""
"Peso de campo '{weight}' inválido, deve ser um número decimal positivo."

#: koha-tmpl/intranet-tmpl/prog/en/includes/html_helpers.inc:268
msgid "Item does not belong to your library"
msgstr "O exemplar não pertence à sua biblioteca"

#: koha-tmpl/intranet-tmpl/prog/en/includes/html_helpers.inc:269
msgid "Item has a waiting hold"
msgstr "O exemplar tem uma reserva em espera"

#: koha-tmpl/intranet-tmpl/prog/en/includes/html_helpers.inc:270
msgid "Item has linked analytics"
msgstr "O exemplar está em uso por analíticos"

#: koha-tmpl/intranet-tmpl/prog/en/includes/html_helpers.inc:267
msgid "Item is checked out"
msgstr "O exemplar está emprestado"

#: koha-tmpl/intranet-tmpl/prog/en/includes/html_helpers.inc:271
msgid "Last item for bibliographic record with biblio-level hold on it"
msgstr "Último exemplar de um registo com uma reserva ao nível de registo"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:63
msgid "Mappings have been reset successfully."
msgstr "Mapeamentos repostos com sucesso."

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:61
msgid "Mappings updated successfully."
msgstr "Mapeamentos atualizados com sucesso."

#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/basket.tt:436
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/basket.tt:639
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/ordered.tt:46
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/parcel.tt:258
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/spent.tt:46
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/transferorder.tt:17
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/transferorder.tt:69
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/uncertainprice.tt:104
msgctxt "noun"
msgid "Order"
msgstr "Encomenda"

#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/newordersubscription.tt:93
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/newordersuggestion.tt:102
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/newordersuggestion.tt:104
#: koha-tmpl/intranet-tmpl/prog/en/modules/acqui/z3950_search.tt:189
msgctxt "verb"
msgid "Order"
msgstr "Encomenda"

#: koha-tmpl/intranet-tmpl/prog/en/modules/course_reserves/course-details.tt:69
#: koha-tmpl/intranet-tmpl/prog/en/modules/course_reserves/course-reserves.tt:52
#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-course-reserves.tt:45
msgctxt "Semester"
msgid "Term"
msgstr "Termo"

#: koha-tmpl/intranet-tmpl/prog/en/modules/course_reserves/course.tt:83
#: koha-tmpl/intranet-tmpl/prog/en/modules/course_reserves/course.tt:96
#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-course-details.tt:44
msgctxt "Semester"
msgid "Term:"
msgstr "Termo:"

#: koha-tmpl/intranet-tmpl/prog/en/modules/catalogue/detail.tt:845
#, perl-brace-format
msgctxt "pluralization"
msgid "There is one archived suggestion."
msgid_plural "There are {count} archived suggestions."
msgstr[0] "Existe uma sugestão arquivada."
msgstr[1] "Existem {count} sugestões arquivadas."

#: koha-tmpl/intranet-tmpl/prog/en/includes/html_helpers.inc:272
msgid "Unknown reason"
msgstr "Causa desconhecida"

#: koha-tmpl/intranet-tmpl/prog/en/modules/members/tables/members_results.tt:22
msgid "View"
msgstr "Ver"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/searchengine/elasticsearch/mappings.tt:53
msgid ""
"You attempted to delete all mappings for a required index, you must leave at "
"least one mapping"
msgstr ""
"Tentou eliminar todos os mapeamentos de um índice obrigatório. Deve deixar "
"pelo menos um mapeamento"

#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-main.tt:228
msgid "checkout"
msgid_plural "checkouts"
msgstr[0] "empréstimo"
msgstr[1] "empréstimos"

#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-main.tt:244
msgid "hold pending"
msgid_plural "holds pending"
msgstr[0] "reserva pendente"
msgstr[1] "reservas pendentes"

#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-main.tt:252
msgid "hold waiting"
msgid_plural "holds waiting"
msgstr[0] "reserva em espera"
msgstr[1] "reservas em espera"

#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-main.tt:269
#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-main.tt:276
msgid "message"
msgid_plural "messages"
msgstr[0] "mensagem"
msgstr[1] "mensagens"

#: koha-tmpl/opac-tmpl/bootstrap/en/modules/opac-main.tt:236
msgid "overdue"
msgid_plural "overdues"
msgstr[0] "atraso"
msgstr[1] "atrasos"

#: koha-tmpl/intranet-tmpl/prog/en/modules/catalogue/results.tt:555
#, perl-brace-format
msgid "{count} item"
msgid_plural "{count} items"
msgstr[0] "{count} item"
msgstr[1] "{count} itens"

#: koha-tmpl/intranet-tmpl/prog/en/modules/admin/credit_types.tt:194
#, perl-brace-format
msgid "{count} library limitation"
msgid_plural "{count} library limitations"
msgstr[0] "{count} limitação da biblioteca"
msgstr[1] "{count} limitações da biblioteca"

#: koha-tmpl/intranet-tmpl/prog/en/modules/course_reserves/batch_rm_items.tt:60
#, perl-brace-format
msgid "{title} ({barcode}) was removed from {count} course."
msgid_plural "{title} ({barcode}) was removed from {count} courses."
msgstr[0] "{title} ({barcode}) foi removido do {count} curso."
msgstr[1] "{title} ({barcode}) foi removido do {count} cursos."
